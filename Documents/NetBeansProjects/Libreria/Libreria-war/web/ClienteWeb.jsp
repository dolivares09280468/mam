<%@ page import="Entidad.*, Stateless.*, java.math.BigDecimal,javax.naming.*, java.util.*"%>
<head>
    <link rel="stylesheet" type="text/css" href="Style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Cliente Web
    </title>
    </head>    
<%!
    private LibroBeanRemote librocat = null;
    String s1, s2, s3;
    Collection list;

    public void jspInit()
    {
        try
        {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando el Catalogo Bean" + librocat);
        } catch (Exception ex)
        {
            System.out.println("Error:" + ex.getMessage());
        }
    }
    public void jspDestroy()
    {
        librocat = null;
    }
%>
<%
    try
    {
        s1 = request.getParameter("t1");
        s2 = request.getParameter("aut");
        s3 = request.getParameter("precio");
        if (s1 != null && s2 != null && s3 != null)
        {
            Double precio = new Double(s3);
            BigDecimal b = new BigDecimal(precio);
            librocat.addLibro(s1, s2, b);
            System.out.println("Registro a�adido:");
%>
<div id="header">
    <p>
        <b>Registro dado de alta</b>
    </p>
</div>
<!--Para insertar en tabla-->
<table align="center" border="1" cellspacing="0" cellpadding="8">
    <tr>
    <th scope="col">
        ID
    </th>
    <th scope="col">
        TITULO
    </th>
    <th scope="col">
        AUTOR
    </th>
    <th scope="col">
        PRECIO
    </th>
</tr>
<%
        }
        list = librocat.getAllLibro();
        for (Iterator iter = list.iterator(); iter.hasNext();)
        {
            Libro elemento = (Libro) iter.next();
%>
<tr>
    <td align="left">
        <%= elemento.getId()%>
    </td>
    <td align="left">
        <%= elemento.getTitulo()%>
    </td>
    <td algin="center">
        <%= elemento.getAutor()%>
    </td>
    <td align="right">
        <%= elemento.getPrecio()%>
    </td>
</tr>
<!--<p> ID: <b><%=elemento.getId()%></b></p>-->
<!--<p>Titulo: <b><%=elemento.getTitulo()%></b></p>-->
<!--<p>Autor: <b><%=elemento.getAutor()%></b></p>-->
<!--<p>Precio: <b><%=elemento.getPrecio()%></b></p>-->
<%
        }
        response.flushBuffer();
    }// fin del try
    catch (Exception e)
    {
        e.printStackTrace();
    }
%>
</table>
<div id="cuerpo1">
    <br/>
    <a href="index.html"><button>Menu principal</button></a><br/>
</div>
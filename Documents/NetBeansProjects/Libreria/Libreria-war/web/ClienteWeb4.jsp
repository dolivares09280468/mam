<%@ page import="Entidad.*, Stateless.*, java.math.BigDecimal,javax.naming.*, java.util.*"%>
<header>
    <link rel="stylesheet" type="text/css" href="Style.css">
    <title>
        Actualizado
    </title>
</header>
<%!
    private LibroBeanRemote librocat = null;
    String s0, s1, s2, s3;
    Collection list;
    Libro libro;
    public void jspInit()
    {
        try
        {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando el Catalogo Bean" + librocat);
        } catch (Exception ex)
        {
            System.out.println("Error:" + ex.getMessage());
        }
    }
    public void jspDestroy()
    {
        librocat = null;
    }
%>
<%
    try
    {
        s0 = request.getParameter("id");
        s1 = request.getParameter("t1");
        s2 = request.getParameter("aut");
        s3 = request.getParameter("precio");
        if (s0 != null)
        {
            Integer id = new Integer(s0);
            librocat.eliminaLibro(id);
            System.out.println("Registro eliminado");
%>
<div id="header">
    <p>
        El registro:<br/>
        <table align="center" border="1" cellspacing="0" cellpadding="8">
            <tr>
                <td>
                    ID:
                </td>
                <td>
                    <%=s0%>
                </td>
            </tr>
            <tr>
                <td>
                    Titulo:
                </td>
                <td>
                    <%=s1%>
                </td>
            </tr>
            <tr>
                <td>
                    Autor:
                </td>
                <td>
                    <%=s2%>
                </td>
            </tr>
            <tr>
                <td>
                    Precio:
                </td>
                <td>
                    <%=s3%>
                </td>
            </tr>
        </table>
    <b>ha sido eliminado</b>
    </p>
</div>
    <%
        }
        list = librocat.getAllLibro();
        for (Iterator iter = list.iterator(); iter.hasNext();)
        {
            Libro elemento = (Libro) iter.next();
    %>
<br/>
<!--<p> ID: <b><%= elemento.getId()%></b></p>-->
<!--<p>Titulo: <b><%= elemento.getTitulo()%></b></p>-->
<!--<p>Autor: <b><%= elemento.getAutor()%></b></p>-->
<!--<p>Precio: <b><%= elemento.getPrecio()%></b></p>-->
<%
        }
    }// fin del try
    catch (Exception e)
    {
        e.printStackTrace();
    }
%>
<div id="cuerpo1">
    <br/>
    <a href="index.html"><button>Menu principal</button></a><br/>
</div>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="Style.css">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Buscar</title>
    </head>
    <body>
        <div id="header">
            <h1>Buscar libro</h1>
            <hr>
        </div>
        <div id="body">
            <form action="ClienteWeb1.jsp" method="POST">
                <table align="center">
                    <tr>
                        <td>
                            Introduzca el id del libro
                        </td>
                        <td>
                            <input type="text" name="id" value="" size="30"/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <br/>
                            <input type="submit" value="Buscar"/>
                        </td>
                        <td align="left">
                            <br/>
                            <input type="reset" value="Limpiar"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>

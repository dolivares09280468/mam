package Stateless;
import Entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Remote;
@Remote
public interface LibroBeanRemote
{
    public void addLibro(String titulo, String autor, BigDecimal precio);
    Collection <Libro> getAllLibro();
    public Libro buscaLibro(int id);
    public void actualizaLibro(Libro libro, String Titulo, String autor, BigDecimal precio);
    public void eliminaLibro(int id);
}